## About Project 1

1. Nomor Kelompok: Group 1 Kelompok 5
2. Link Video Demo: https://youtu.be/92kO4WpyJJo
3. Link Deploy: http://project-laravel-1.herokuapp.com/ 

## ERD
<img src="https://gitlab.com/rootercrack/project-laravel-1/-/raw/master/public/img/erd.jpg">


## Langkah-langkah Git Clone project pertama kali:
1. buat folder kosong lalu masuk ke folder tersebut
2. git clone https://gitlab.com/rootercrack/project-laravel-1.git
3. git checkout master
4. composer update
5. composer install
6. buat database kosong di phpmyadmin
7. rename .env.example jadi .env atau create file .env
8. sesuaikan isi .env dengan DB yang ada
- DB_CONNECTION=mysql
- DB_HOST=127.0.0.1
- DB_PORT=3306
- DB_DATABASE=namadatabase
- DB_USERNAME=root
- DB_PASSWORD=
9. php artisan key:generate
10. php artisan migrate
11. php artisan serve

## Langkah-langkah Git Pull project kedua dst:
1. git pull https://gitlab.com/rootercrack/project-laravel-1.git
2.  git checkout master

## Langkah-langkah Git Push
1. git pull
2. git add .
3. git commit -m "Isi Apa yang di Update"
4. git push -u origin master

