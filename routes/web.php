<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'CutiController@index');
Route::get('/cuti','CutiController@index');

Route::group(['middleware' => ['auth']], function(){


    //Departemen
    Route::get('/departemen','DepartemenController@index'); 
    Route::get('/departemen/create','DepartemenController@create'); 
    Route::post('/departemen','DepartemenController@store');
    Route::get('/departemen/{departemen_id}','DepartemenController@show');
    Route::get('/departemen/{departemen_id}/edit','DepartemenController@edit');
    Route::put('/departemen/{departemen_id}','DepartemenController@update');
    Route::delete('/departemen/{departemen_id}','DepartemenController@destroy');

    //Bio Karyawan
    /*
    Route::get('/biokaryawan','BiokaryawanController@index'); 
    Route::get('/biokaryawan/create','BiokaryawanController@create'); 
    Route::post('/biokaryawan','BiokaryawanController@store');
    Route::get('/biokaryawan/{biokaryawan_id}','BiokaryawanController@show');
    Route::get('/biokaryawan/{biokaryawan_id}/edit','BiokaryawanController@edit');
    Route::put('/biokaryawan/{biokaryawan_id}','BiokaryawanController@update');
    Route::delete('/biokaryawan/{biokaryawan_id}','BiokaryawanController@destroy'); */

    Route::resource('biokaryawan', 'BiokaryawanController')->only([
        'index', 'update', 'edit'
    ]);

    //Form Cuti 
    Route::get('/cuti/create','CutiController@create'); 
    Route::post('/cuti','CutiController@store');
    Route::get('/cuti/{cuti_id}','CutiController@show');
    Route::get('/cuti/{cuti_id}/edit','CutiController@edit');
    Route::put('/cuti/{cuti_id}','CutiController@update');
    Route::delete('/cuti/{cuti_id}','CutiController@destroy');
});



Auth::routes();


