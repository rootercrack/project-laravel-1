<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cuti;

class CutiController extends Controller
{
    public function create() {
        return view('cuti.create');
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $request->validate([
            'tanggal_cuti' => 'required|date',
            'tanggal_masuk' => 'required|date',
            'lama_cuti' => 'required|integer',
            'alasan_cuti' => 'required|max:255',
            'user_id' => 'required|integer'
            
        ],
        [
            'tanggal_cuti.required' => 'Mohon Isi Tanggal Awal Anda Mengambil Cuti',
            'tanggal_cuti.date' => 'Tanggal Salah',
            'tanggal_masuk.required' => 'Mohon Isi Tanggal Anda Masuk Kerja',
            'tanggal_masuk.date' => 'Tanggal Salah',
            'lama_cuti.required' => 'Isi Berapa Hari Kerja Anda Mengambil Cuti',
            'lama_cuti.integer' => 'Isi Berapa Jumlah Hari Anda Mengambil Cuti',
            'alasan_cuti.required' => 'Mohon Isi Alasan Cuti Anda',
            'alasan_cuti.max' => 'Maksimum 255 karakter',
            'user_id.required' => 'Isi User ID Anda',
            'user_id.integer' => 'User ID Anda Salah'
        ]);

        $cuti = new Cuti;
        $cuti->tanggal_cuti = $request->tanggal_cuti;
        $cuti->tanggal_masuk = $request->tanggal_masuk;
        $cuti->lama_cuti = $request->lama_cuti;
        $cuti->alasan_cuti = $request->alasan_cuti;
        $cuti->user_id = $request->user_id;
        $cuti->save();

        return redirect('/cuti');
    }

    public function index()
    {
        $cuti = Cuti::all();
        return view('cuti.index', compact('cuti'));
    }

    public function show($id)
    {
        $cuti = Cuti::find($id)->first();
        return view('cuti.show', compact('cuti'));
    }

    public function edit($id)
    {
        $cuti = Cuti::find($id);
        return view('cuti.edit', compact('cuti'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $request->validate([
            'tanggal_cuti' => 'required|date',
            'tanggal_masuk' => 'required|date',
            'lama_cuti' => 'required|integer',
            'alasan_cuti' => 'required|max:255',
            
        ],
        [
            'tanggal_cuti.required' => 'Mohon Isi Tanggal Awal Anda Mengambil Cuti',
            'tanggal_cuti.date' => 'Tanggal Salah',
            'tanggal_masuk.required' => 'Mohon Isi Tanggal Anda Masuk Kerja',
            'tanggal_masuk.date' => 'Tanggal Salah',
            'lama_cuti.required' => 'Isi Berapa Hari Kerja Anda Mengambil Cuti',
            'lama_cuti.integer' => 'Isi Berapa Jumlah Hari Anda Mengambil Cuti',
            'alasan_cuti.required' => 'Mohon Isi Alasan Cuti Anda',
            'alasan_cuti.max' => 'Maksimum 255 karakter',
        ]);

        $cuti = Cuti::find($id);
        $cuti->tanggal_cuti = $request->tanggal_cuti;
        $cuti->tanggal_masuk = $request->tanggal_masuk;
        $cuti->lama_cuti = $request->lama_cuti;
        $cuti->alasan_cuti = $request->alasan_cuti;

        $cuti->save();

        return redirect('/cuti');
    }

    public function destroy($id)
    {
        $cuti = Cuti::find($id);
        $cuti->delete();

        return redirect('/cuti');
    }

}
