<?php

namespace App\Http\Controllers;
use App\Departemen;
use Illuminate\Http\Request;

class DepartemenController extends Controller
{
    public function create() {
        return view('departemen.create');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required',
            'lokasi' => 'required',
            
        ]);
  
        $departemen = new departemen;
  
        $departemen->nama = $request['nama'];
        $departemen->lokasi = $request['lokasi'];
  
  
        $departemen->save();
  
        return redirect('/departemen');
  
        
     }

public function index(){
    $departemen = departemen::all();
    return view('departemen.index', compact('departemen'));

 }
 public function show($departemen_id)
 {
     $departemen = Departemen::where('id', $departemen_id)->first();
     return view('departemen.show', compact('departemen'));
 }
 public function edit($departemen_id)
 {
     $departemen = Departemen::where('id', $departemen_id)->first();
     return view('departemen.edit', compact('departemen'));
 }
 public function update (Request $request, $departemen_id){
    $request->validate([
        'nama' => 'required',
        'lokasi' => 'required',
        
    ]);

    $departemen = Departemen::find($departemen_id);

    $departemen->nama = $request['nama'];
    $departemen->lokasi = $request['lokasi'];


    $departemen->save();
    return redirect('/departemen');
 }
 public function destroy($departemen_id)
    {
        $departemen = Departemen::find($departemen_id);
 
        $departemen->delete();

        return redirect('/departemen');
    }


     
}

 
