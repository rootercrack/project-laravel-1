<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Biokaryawan;

class BiokaryawanController extends Controller
{
    public function create()
    {
        return view('biokaryawan.create');
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'alamat' => 'required',
            'umur' => 'required',
            'bio' => 'required',
            'user_id' => 'required',
            'departemen_id' => 'required',
        ],
        [
            'alamat.required' => 'Alamat harus diisi',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi',
            'user_id.required' => 'ID User harus diisi',
            'departemen_id.required' => 'ID Departemen harus diisi',
        ]);
        
        $biokaryawan = new Biokaryawan;
 
        $biokaryawan->alamat = $request->alamat;
        $biokaryawan->umur = $request->umur;
        $biokaryawan->bio = $request->bio;
        $biokaryawan->user_id = $request->user_id;
        $biokaryawan->departemen_id = $request->departemen_id;
 
        $biokaryawan->save();

        return redirect('/biokaryawan');
    }

    public function index()
    {
        // $biokaryawan = Biokaryawan::all();
        $biokaryawan = Biokaryawan::where('user_id', Auth::id())->first();
        //dd($biokaryawan);
        //dd(Auth::id());
        return view('biokaryawan.edit', compact('biokaryawan'));

        

    }

    public function show($biokaryawan_id)
    {
        $biokaryawan = Biokaryawan::where('id', $biokaryawan_id)->first();
        return view('biokaryawan.show', compact('biokaryawan'));
    }

    public function edit($biokaryawan_id)
    {
        $biokaryawan = Biokaryawan::where('id', $biokaryawan_id)->first();
        return view('biokaryawan.edit', compact('biokaryawan'));
    }

    public function update(Request $request, $biokaryawan_id)
    {
        $request->validate([
            'alamat' => 'required',
            'umur' => 'required',
            'bio' => 'required',

        ],
        [
            'alamat.required' => 'Alamat harus diisi',
            'umur.required' => 'Umur harus diisi',
            'bio.required' => 'Bio harus diisi',

        ]);

        $biokaryawan = Biokaryawan::find($biokaryawan_id);
 
        $biokaryawan->alamat = $request['alamat'];
        $biokaryawan->umur = $request['umur'];
        $biokaryawan->bio = $request['bio'];
        
        $biokaryawan->save();

        return redirect('/biokaryawan');
    }

    public function destroy($biokaryawan_id)
    {
        $biokaryawan = Biokaryawan::find($biokaryawan_id);
 
        $biokaryawan->delete();

        return redirect('/biokaryawan');
    }
}
