<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biokaryawan extends Model
{
    protected $table = 'bio_karyawan';
    protected $fillable = ['alamat', 'umur', 'bio', 'user_id', 'departemen_id'];

    public function user() {
        return $this->belongsTo('App\User');
    }

    public function departemen() {
        return $this->belongsTo('App\Departemen');
    }

}
