<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cuti extends Model
{
    protected $table = 'form_cuti';
    protected $fillable = ['tanggal_cuti', 'tanggal_masuk', 'lama_cuti', 'alasan_cuti', 'user_id'];
    
    public function user() {
        return $this->belongsTo('App\User');
    }
}
