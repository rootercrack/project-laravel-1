@extends('layouts.master')
@section('title')
Cuti
@endsection
@section('title2')
Edit Cuti
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/6enkfaiemzyko9ep97a8qk08eo2v3rd2uowhgmta6bt4mm1j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
</script>
@endpush
@section('content')

<form method="POST" action="/cuti/{{$cuti->id}}">
    @csrf
    @method('put')

    <div class="form-group">
        <label for="user_id">User ID</label>
        <input type="text" class="form-control" value="{{$cuti->user->name}}" disabled>
    </div>


    <div class="form-group">
        <label for="tanggal_cuti">Tanggal Cuti</label>
        <input type="date" class="form-control" id="tanggal_cuti" name="tanggal_cuti" value="{{$cuti->tanggal_cuti}}">
    </div>
    @error('tanggal_cuti')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="tanggal_masuk">Tanggal Masuk</label>
        <input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk" value="{{$cuti->tanggal_masuk}}">
    </div>
    @error('tanggal_masuk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="lama_cuti">Lama Cuti</label>
        <input type="number" class="form-control" id="lama_cuti" name="lama_cuti" value="{{$cuti->lama_cuti}}">
    </div>
    @error('lama_cuti')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alasan Cuti</label>
        <textarea name="alasan_cuti" cols="30" rows="10" class="form-control" id="mytextarea">{{$cuti->alasan_cuti}}</textarea>
    </div>
    @error('alasan_cuti')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror





    <button type="submit" class="btn btn-primary">Edit</button>
</form>

@endsection