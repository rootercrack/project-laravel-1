@extends('layouts.master')
@section('title')
Cuti
@endsection
@section('title2')
Detil Cuti
@endsection
@section('content')
<p>
    Tanggal Cuti: {{$cuti->tanggal_cuti}}<br>
    Tanggal Masuk: {{$cuti->tanggal_masuk}}<br>
    Lama Cuti: {{$cuti->lama_cuti}}<br>
    User: {{$cuti->user->name}}<br>
</p>
@endsection