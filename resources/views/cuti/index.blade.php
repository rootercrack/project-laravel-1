@extends('layouts.master')
@section('title')
Cuti
@endsection
@section('title2')
Daftar Cuti
@endsection
@section('content')

@auth
<a href="/cuti/create" class="btn btn-secondary mb-3">Tambah Cuti</a>
@endauth

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">User</th>
      <th scope="col">Tanggal Cuti</th>
      <th scope="col">Tanggal Masuk</th>
      <th scope="col">Lama Cuti</th>
      <th scope="col">Alasan Cuti</th>
      @auth
      <th scope="col">Action</th>
      @endauth
    </tr>
  </thead>
  <tbody>
      @forelse ($cuti as $key => $item)
            <tr>
                <td>{{$key + 1 }}</td>
                <td>{{$item ->user->name}}</td>
                <td>{{$item ->tanggal_cuti}}</td>
                <td>{{$item ->tanggal_masuk}}</td>
                <td>{{$item ->lama_cuti}}</td>
                <td>{{$item ->alasan_cuti}}</td>
                @auth
                <td>
                    
                    <form action="/cuti/{{$item->id}}" class="mt-2" method="POST">
                        @csrf
                        @method('delete')
                        <a href="/cuti/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                        <a href="/cuti/{{$item->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                        <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                    </form>
                    
                </td>
                @endauth
            </tr>
      @empty
          <h1>Data tidak ada</h1>
      @endforelse
  </tbody>
</table>




@endsection