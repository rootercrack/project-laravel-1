@extends('layouts.master')
@section('title')
Cuti
@endsection
@section('title2')
Form Cuti
@endsection
@push('script')
<script src="https://cdn.tiny.cloud/1/6enkfaiemzyko9ep97a8qk08eo2v3rd2uowhgmta6bt4mm1j/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>
<script>
  tinymce.init({
    selector: '#mytextarea'
  });
</script>
@endpush

@section('content')

<form action="/cuti" method="POST">
    @csrf
    
    <div class="form-group">
        <label for="tanggal_cuti">Tanggal Cuti</label>
        <input type="date" class="form-control" id="tanggal_cuti" name="tanggal_cuti">
    </div>
    @error('tanggal_cuti')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="tanggal_masuk">Tanggal Masuk</label>
        <input type="date" class="form-control" id="tanggal_masuk" name="tanggal_masuk">
    </div>
    @error('tanggal_masuk')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="lama_cuti">Lama Cuti</label>
        <input type="number" class="form-control" id="lama_cuti" name="lama_cuti">
    </div>
    @error('lama_cuti')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Alasan Cuti</label>
        <textarea name="alasan_cuti" cols="30" rows="10" class="form-control" id="mytextarea"></textarea>
    </div>
    @error('alasan_cuti')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label for="user_id">User ID</label>
        <input type="number" class="form-control" id="user_id" name="user_id">
    </div>
    @error('user_id')
        <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection