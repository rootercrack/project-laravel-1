@extends('layouts.master')
@section('title')
Departemen
@endsection
@section('content')

<a href="/departemen/create" class="btn btn-secondary mb-3">Tambah Departemen</a>
@csrf
<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama</th>
      <th scope="col">Lokasi</th>
      <th scope="col">Action</th>
      
    </tr>
  </thead>
  <tbody>
      @forelse ($departemen as $key => $item)
            <tr>
                <td>{{$key + 1 }}</td>
                <td>{{$item ->nama}}</td>
                <td>{{$item ->lokasi}}</td>
                <td>
                <form action="/departemen/{{$item->id}}" method="POST"> 
                <a href='/departemen/{{$item ->id}}' class='btn btn-info btn-sm'>Detail</a>
                <a href="/departemen/{{$item ->id}}/edit" class='btn btn-warning btn-sm'>edit</a>
                @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
              </tr>
             
      @empty
          <h3>Departemen tidak ada</h3>
      @endforelse
      
  </tbody>
</table>



@endsection     

