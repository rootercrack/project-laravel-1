@extends('layouts.master')
@section('title')
Departemen
@endsection
@section('title2')
Form Departemen
@endsection
@section('content')

<form method="POST" action="/departemen">
    @csrf

    <div class="form-group">
        <label>Nama</label>
        <input type="text" name="nama"  class="form-control">
    </div>
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Lokasi</label>
        <input type="text" name="lokasi" class="form-control">
    </div>
    @error('lokasi')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection
