@extends('layouts.master')
@section('title')
List Bio Karyawan
@endsection
@section('content')

<a href="/biokaryawan/create" class="btn btn-secondary mb-3">Tambah Bio Karyawan</a>

<table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Alamat</th>
      <th scope="col">Umur</th>
      <th scope="col">Bio</th>
      <th scope="col">User</th>
      <th scope="col">Departemen</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>
      @forelse ($biokaryawan as $key => $item)
            <tr>
                <td>{{$key + 1 }}</td>
                <td>{{$item ->alamat}}</td>
                <td>{{$item ->umur}}</td>
                <td>{{$item ->bio}}</td>
                <td>{{$item ->user_id}}</td>
                <td>{{$item ->departemen_id}}</td>
                <td>
                    <form action="/biokaryawan/{{$item->id}}" method="POST">
                        <a href='/biokaryawan/{{$item ->id}}' class='btn btn-info btn-sm'>Detail</a>
                        <a href="/biokaryawan/{{$item ->id}}/edit" class='btn btn-warning btn-sm'>Edit</a>
                        @csrf
                        @method('delete')
                        <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                    </form>
                </td>
            </tr>
      @empty
          <h1>Data tidak ada</h1>
      @endforelse
  </tbody>
</table>




@endsection