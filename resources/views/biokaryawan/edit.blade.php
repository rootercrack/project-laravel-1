@extends('layouts.master')
@section('title')
Edit Bio Karyawan
@endsection
@section('content')

<form action="/biokaryawan/{{$biokaryawan->id}}" method="POST">
    @csrf
    @method("PUT")

    <div class="form-group">
        <label>Nama User</label>
        <input type="text" value="{{$biokaryawan->user->name}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Email</label>
        <input type="text" value="{{$biokaryawan->user->email}}" class="form-control" disabled>
    </div>

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" cols="30" rows="10" class="form-control">{{$biokaryawan->alamat}}</textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" value="{{$biokaryawan->umur}}" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control">{{$biokaryawan->bio}}</textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Departemen</label>
        <input type="text" value="{{$biokaryawan->departemen->nama}}" class="form-control" disabled>
    </div>
    @error('departemen_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>

</form>


@endsection