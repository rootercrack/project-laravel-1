@extends('layouts.master')
@section('title')
Bio Karyawan
@endsection
@section('title2')
Form Bio Karyawan
@endsection
@section('content')

<form action="/biokaryawan" method="POST">
    @csrf

    <div class="form-group">
        <label>Alamat</label>
        <textarea name="alamat" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('alamat')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Umur</label>
        <input type="number" name="umur" class="form-control">
    </div>
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
    </div>
    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>User</label>
        <input type="number" name="user_id" class="form-control">
    </div>
    @error('user_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
        <label>Departemen</label>
        <input type="number" name="departemen_id" class="form-control">
    </div>
    @error('departemen_id')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
    <button type="submit" class="btn btn-primary">Submit</button>

</form>

@endsection