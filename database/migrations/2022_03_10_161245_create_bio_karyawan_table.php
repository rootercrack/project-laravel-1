<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBioKaryawanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bio_karyawan', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('alamat');
            $table->integer('umur');
            $table->text('bio');
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('departemen_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('departemen_id')->references('id')->on('departemen');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bio_karyawan');
    }
}
